﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tasks
{
#pragma warning disable S1118 // Utility classes should not have public constructors
    public class TaskChaining
#pragma warning restore S1118 // Utility classes should not have public constructors
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World");
            Console.WriteLine("Generated array");
            Task<int[]> task1 = Task.Run(() => generateArray());
            foreach (int item in task1.Result)
            {
                Console.Write(item + " ");
            }

            Console.WriteLine("");
            Console.WriteLine("Multipled array");
            Task<int[]> task2 = task1.ContinueWith(x => multiplyArray(task1.Result));
            foreach (int item in task2.Result)
            {
                Console.Write(item + " ");
            }

            Console.WriteLine("");
            Console.WriteLine("Sorted array");
            Task<int[]> task3 = task2.ContinueWith(x => sortedArray(task2.Result));
            foreach (int item in task3.Result)
            {
                Console.Write(item + " ");
            }

            Console.WriteLine("");
            Console.WriteLine("Average");
            Task<double> task4 = task3.ContinueWith(x => average(task3.Result));
            Console.Write(task4.Result);

            Console.ReadKey();
        }

        public static int[] generateArray()
        {
            Random rn = new Random();
            int[] randomNumbers = new int[10];
            for (int i = 0; i < 10; i++)
            {
                randomNumbers[i] = rn.Next();
            }

            return randomNumbers;
        }

        public static int[] multiplyArray(int[] numberArray, int myRandom = 0)
        {
            Validation(numberArray);

            Random rn = new Random();
            int randomNumber;
            if (myRandom != 0)
            {
                randomNumber = myRandom;
            }
            else
            {
                randomNumber = rn.Next();
            }

            for (int i = 0; i < 10; i++)
            {
                numberArray[i] = numberArray[i] * randomNumber;
            }

            return numberArray;
        }

        public static int[] sortedArray(int[] numberArray)
        {
            Validation(numberArray);
            Array.Sort(numberArray);
            return numberArray;
        }

        public static double average(int[] numberArray)
        {
            Validation(numberArray);
            return numberArray.Average();
        }

        public static void Validation(int[] numberArray)
        {
            if (numberArray.Length == 0)
            {
                throw new ArgumentException("Array can't be empty", nameof(numberArray));
            }

            if (numberArray.Length > 10)
            {
                throw new ArgumentOutOfRangeException(nameof(numberArray));
            }
        }
    }
}
